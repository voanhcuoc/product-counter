#define a1 2
#define b1 3
#define c1 4
#define d1 5
#define e1 6
#define f1 7
#define g1 8

#define a2 9
#define b2 10
#define c2 11
#define d2 12
#define e2 13
#define f2 A0
#define g2 A1

#define sensor    A2
#define reset     A3
#define toggle    A4
#define enableLED A5

int last1 = 0, state1 = 0;
int last2 = 0, state2 = 0;
int last3 = 0, state3 = 0;

int count = 0;

bool enable = true;

void seg1(int a, int b, int c, int d, int e, int f, int g);
void seg2(int a, int b, int c, int d, int e, int f, int g);
void Segment_Display(int Seg_1, int Seg_2);

void setup() 
{
  // put your setup code here, to run once:
   Serial.begin(9600);
    pinMode(a1, OUTPUT);
    pinMode(b1, OUTPUT);
    pinMode(c1, OUTPUT);
    pinMode(d1, OUTPUT);
    pinMode(e1, OUTPUT);
    pinMode(f1, OUTPUT);
    pinMode(g1, OUTPUT);

    pinMode(a2, OUTPUT);
    pinMode(b2, OUTPUT);
    pinMode(c2, OUTPUT);
    pinMode(d2, OUTPUT);
    pinMode(e2, OUTPUT);
    pinMode(f2, OUTPUT);
    pinMode(g2, OUTPUT);

    pinMode(sensor, INPUT);
    pinMode(reset, INPUT);
    pinMode(toggle, INPUT);
}

#define SENSOR_SENSIBILITY 20
#define BUTTON_SENSIBILITY 30
void loop() 
{
  delay(5);
  // put your main code here, to run repeatedly:
  int now = millis();
  int s = digitalRead(sensor);
  if ((s==0) && (state1==1)) {
    last1 = now;
  } else if ((s==1) && (state1==0) && (now-last1 > SENSOR_SENSIBILITY)) {
    if (enable && (count < 99)) {
      count++;
    }
  }
  state1 = s;

  int r = digitalRead(reset);
  if ((r==0) && (state2==1)) {
    last2 = now;
  } else if ((r==1) && (state2==0) && (now-last2 > BUTTON_SENSIBILITY)) {
    count = 0;
  }
   state2 = r;

  int e = digitalRead(toggle);
  if ((e==0) && (state3==1)) {
    last3 = now;
  } else if ((e==1) && (state3==0) && (now-last3 > BUTTON_SENSIBILITY)) {
    enable = !enable;
  }
  state3 = e;

  digitalWrite(enableLED, enable ? 1 : 0);
  Segment_Display(count/10, count%10);
}

void seg1(int a, int b, int c, int d, int e, int f, int g) {
  digitalWrite(a1, a);
  digitalWrite(b1, b);
  digitalWrite(c1, c);
  digitalWrite(d1, d);
  digitalWrite(e1, e);
  digitalWrite(f1, f);
  digitalWrite(g1, g);
}

void seg2(int a, int b, int c, int d, int e, int f, int g) {
  digitalWrite(a2, a);
  digitalWrite(b2, b);
  digitalWrite(c2, c);
  digitalWrite(d2, d);
  digitalWrite(e2, e);
  digitalWrite(f2, f);
  digitalWrite(g2, g);
}

void Segment_Display(int Seg_1, int Seg_2)
{
  switch(Seg_1)
  {
    case 0: 
    {
      seg1(0, 0, 0, 1, 0, 0, 0);
      break;
    }
    case 1: 
    {
      seg1(1, 1, 0, 1, 1, 1, 0);
      break;
    }
    case 2: 
    {
      seg1(0, 0, 1, 0, 1, 0, 0);
      break;
    }
    case 3: 
    {
      seg1(1,0,0,0,1,0,0);
      break;
    }
    case 4: 
    {
      seg1(1,1,0,0,0,1,0);
      break;
    }
    case 5: 
    {
      seg1(1,0,0,0,0,0,1);
      break;
    }
    case 6: 
    {
      seg1(0,0,0,0,0,0,1);
      break;
    }
    case 7: 
    {
      seg1(1,1,0,1,1,0,0);
      break;
    }
    case 8: 
    {
      seg1(0,0,0,0,0,0,0);
      break;
    }
    case 9: 
    {
      seg1(1,0,0,0,0,0,0);
      break;
    }
    
    default:{};
  }

  switch(Seg_2)
  {
    case 0: 
    {
      seg2(0,0,0,0,1,0,0);
      break;
    }
    case 1: 
    {
      seg2(1,1,0,1,1,0,1);
      break;
    }
    case 2: 
    {
      seg2(1,0,0,0,0,1,0);
      break;
    }
    case 3: 
    {
      seg2(1,0,0,1,0,0,0);
      break;
    }
    case 4: 
    {
      seg2(0,1,0,1,0,0,1);
      break;
    }
    case 5: 
    {
      seg2(0,0,1,1,0,0,0);
      break;
    }
    case 6: 
    {
      seg2(0,0,1,0,0,0,0);
      break;
    }
    case 7: 
    {
      seg2(1,0,0,1,1,0,1);
      break;
    }
    case 8: 
    {
      seg2(0,0,0,0,0,0,0);
      break;
    }
    case 9: 
    {
      seg2(0,0,0,1,0,0,0);
      break;
    }
    
    default:{};
  }
}
